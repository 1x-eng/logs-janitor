from pino import pino

logger = pino(
    messagekey="msg",
    millidiff=False
    # bindings={"apptype": "prototype", "context": "main"}
)

#  UUID / Device Identifier
logger.info({ "key": "value" }, "The device/hub ID is: 835b8354-26d1-4d17-b318-520a893c4d3b")
# IP Address
logger.info({ "key": "value" }, "Device IP Address is = 172.191.144.16")
# MAC Address
logger.info({ "key": "value" }, "Device MAC Address is - 00:27:0e:2a:b9:aa")
# Email Address
logger.info({ "key": "value" }, "Device email address is somehub@someemail.com")
# Medicare Number
# logger.info({ "key": "value" }, "Random medicare number 5101 20591 8-1")
# dob
logger.info({ "key": "value" }, "Random patient dob is 31/10/2020, 21-03-1979, 1979/03/21, 1979-03-21")
# phone number
logger.info({ "key": "value" }, "Random patient phone number = +(61) 455 562 400, 0414570776, +61444999333")
# SSN
logger.info({ "key": "value" }, "Random patient SSN = 441-52-7994")
# URL
logger.info({ "key": "value" }, "Clinic URL = https://someemail.com, http://someemail.com, www.someemail.org")
# Credit card number
logger.info({ "key": "value" }, "Random patient visa credit card number = 4556916539314978, mastercard = 5281351412467520, amex = 345460880850953")

