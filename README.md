# Deidentify PII/PHI from application logs | Logs Janitor

![workflow](./static/logs_deidentification.jpg)

## Getting started
- Install latest version of NodeJS.
- `git clone https://gitlab.com/1x-eng/logs-janitor.git`
- `cd logs-janitor && npm install && npm start`
- `examples` directory contain simple nodejs & python examples.
- [In a separate terminal] To sanitize logs from nodejs example - `node examples/nodejs/pino_logs.js | node dist/index.js`
- [In a separate terminal] To sanitize logs from pythn example - `pip install -r examples/python/requirements.txt && python examples/python/pino_logs.py | node dist/index.js` 

## Interpreting output
Sample output: 
```
log object due for transformation = {
  level: 'info',
  time: 1655269741458,
  msg: 'The device/hub ID is: 835b8354-26d1-4d17-b318-520a893c4d3b',
  host: 'MacBook-Pro-4.local',
  key: 'value'
}
log object post transformation = {
  level: 'info',
  time: 1655269741458,
  msg: 'The device/hub ID is: :uuid',
  host: 'MacBook-Pro-4.local',
  key: 'value'
}
```

The first log is what the parent process has logged to `stdout`. The second one is post treatment from `logs-janitor`. the `UUID` in `msg` is replaced with a placeholder.
