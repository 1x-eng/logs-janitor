import split from "split2";
import pump from "pump";
import through, { TransformCallback } from "through2";
import { LogChunk, LogObject } from "./types";
import { sanitize } from "./transformers";


const isLogObject = (chunk: LogChunk): chunk is LogObject => {
    return (chunk as LogObject).level !== undefined;  
}

const transformFunction = (chunk: LogChunk, encoding: BufferEncoding, callback: TransformCallback): void => {
    if (isLogObject(chunk)) {
        sanitize(chunk);
        callback();
        return;
    }
    console.log(`Primitive log = ${chunk}`);
    callback();
}

const LogTransport = through
.obj(transformFunction);

pump(process.stdin, split(JSON.parse), LogTransport);
