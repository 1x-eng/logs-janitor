// Agreed contract for logs from parent process
// Maybe better to have this as an interface and extend it with service specific types?
export type LogObject = {
    level: number,
    time: bigint,
    pid: number,
    hostname: string,
    msg?: string
    [key: string]: unknown,
}

export type LogChunk = LogObject | string | number;