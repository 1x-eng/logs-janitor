import { LogObject } from "./types";
import { deepClone }  from "./util"


export const sanitize = (chunk: LogObject): LogObject => {
    // PHI Identifiers = https://cphs.berkeley.edu/hipaa/hipaa18.html

    console.log(`log object due for transformation =`, chunk);
    let chunkClone = deepClone(chunk);
    chunkClone.msg = chunk.msg?.replace(
        // UUID / Device identifiers
        /[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}/gi,
      ":uuid"
    ).replace(
        // IP Address
        new RegExp("\\b(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\b", 'gi'),
        ":ipaddress"
    ).replace(
        // MAC Address
        /((([a-zA-z0-9]{2}[-:]){5}([a-zA-z0-9]{2}))|(([a-zA-z0-9]{2}:){5}([a-zA-z0-9]{2})))/,
        ":macaddress"
    ).replace(
        // Email
        /([^.@\s]+)(\.[^.@\s]+)*@([^.@\s]+\.)+([^.@\s]+)/,
        ":email"
    ).replace(
        // Australian Medicare
        /^[2-6]\d{3}[ ]?\d{5}[ ]?\d{1}[-]?\d?$/,
        ":au_medicare_number"
    ).replace(
        // Dates
        /((?=\d{4})\d{4}|(?=[a-zA-Z]{3})[a-zA-Z]{3}|\d{2})((?=\/)\/|\-)((?=[0-9]{2})[0-9]{2}|(?=[0-9]{1,2})[0-9]{1,2}|[a-zA-Z]{3})((?=\/)\/|\-)((?=[0-9]{4})[0-9]{4}|(?=[0-9]{2})[0-9]{2}|[a-zA-Z]{3})/g,
        ":date"
    ).replace(
        // Phone number
        /\b([\+-]?\d{2}|\d{4})\s*\(?\d+\)?\s*(?:\d+\s*)+\b/g,
        ":phone_number"
    ).replace(
        // SSN
        /^(?!000)(?!666)(?!9)\d{3}([- ]?)(?!00)\d{2}\1(?!0000)\d{4}$/,
        ":social_security_number"
    ).replace(
        // URLs starting with http://, https:// or ftp://
        /(\b(https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gim,
        ":http_address"
    ).replace(
        // URLs starting with "www."
        /(^|[^\/])(www\.[\S]+(\b|$))/gim,
        ":www_address"
    ).replace(
        // Credit cards - https://stackoverflow.com/a/9315696
        /^(?:4[0-9]{12}(?:[0-9]{3})?|[25][1-7][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$/,
        ":credit_card"
    );
    console.log(`log object post transformation =`, chunkClone);
    return chunkClone;
}

