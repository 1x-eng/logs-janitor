export function deepClone<T>(obj: T): T {
    // Caution: Date type will not be preserved.
    return JSON.parse(JSON.stringify(obj)) as T;
}